import React, { useState } from "react";
import MainDash from "../../../../src/components/DashContainers/MainDash";
import TabLayout from "../../../../src/components/Taboptions/TabLayout";
import ServerOverview from "../../../../src/components/Server/Server";
import GroupServer from "../../../../src/components/Group/group";
import ServerSnippet from "../../../../src/components/ServerSnippet/ServerSnipet";
import { UserRoutes } from "../../../../util.js/routes";

const Overview = () => {
  const [activeTab, setActiveTab] = useState("Servers");
  return (
    <MainDash menu={UserRoutes}>
      {/*  <ServerOverview /> */}
      <TabLayout activeTab={activeTab} setActiveTab={setActiveTab} />
      {getCurrentScreen(activeTab)}
    </MainDash>
  );
};

const getCurrentScreen = (tab) => {
  console.log(tab, "from tabs switch");
  switch (tab) {
    case "Servers":
      return <ServerOverview />;
    case "Groups":
      return <GroupServer />;
    case "ServerSnipet":
      return <ServerSnippet />;
    default:
    // return <ServerOverview />;
  }
};

export default Overview;
