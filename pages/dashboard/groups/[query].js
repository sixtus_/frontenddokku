import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import ServerDash from "../../../src/components/DashContainers/ServerDash";
import { getSpecifiedgroup, group } from "../../../src/components/Group/group";
// import { getSpecifiedGroup, groups } from "../util.js/serverUtil";

export default function Query() {
  const router = useRouter();
  const { query } = router.query;
  const [group, setGroup] = useState([]);

  useEffect(() => {
    ({ group: group, id: query, setGroup });
  }, []);

  const Foundserver = group && (
    <div>
      <h2>{group.groupName}</h2>
      <span>{group._id}</span>
      <div>{group.description}</div>
    </div>
  );
  return (
    <ServerDash>
      hello hello
      <div style={{ backgroundColor: "#FFFFFF" }}>
        <h2>{group.groupName} hello</h2>
        <span>{group._id}</span>
        <div>{group.description}</div>
      </div>
    </ServerDash>
  );
}
