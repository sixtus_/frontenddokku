import React from "react";
import Navbar from "../src/components/LandingLayout/navbar/Navbar";
import Footer from "../src/components/LandingLayout/Footer/Footer";
import PageHeader from "../src/components/LandingLayout/Header/Header";
import HomeTabs from "../src/components/LandingLayout/landingpage/Homepage";

export default function Landingpage() {
  return (
    <div>
      <Navbar />
      <PageHeader />
      <HomeTabs />
      <Footer />
    </div>
  );
}
