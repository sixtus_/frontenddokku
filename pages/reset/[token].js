import React from "react";

import CheckAuthForUnAuthPages from "../../src/components/AuthControl/checkAuthForUnAuthPages";
import ResetPassword from "../../src/components/ForgotPassword/ResetPassword";

export default function ResetPasswordPage(props) {
  return (
    <CheckAuthForUnAuthPages>
      <ResetPassword {...props}/>
    </CheckAuthForUnAuthPages>
  );
}
