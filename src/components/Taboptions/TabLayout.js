import React from "react";
import { Tabs } from "./styled.TabLayout";

const tabs = [
  {
    id: 1,
    name: "Servers",
    backdrop: "#FCEEC0",
  },
  {
    id: 2,
    name: "Groups",
    backdrop: "#E5f3F3",
  },
  {
    id: 3,
    name: "ServerSnipet",
    backdrop: "#E5f3F3",
  },
];

const TabLayout = ({ activeTab, setActiveTab }) => {
  return (
    <Tabs className="tab-options">
      <div className="max-width options-wrapper">
        {tabs.map((tab) => {
          return (
            <div
              onClick={() => setActiveTab(tab.name)}
              className={`tab-item absolute-center cursor 
          ${activeTab === tab.name && "active-tab"}`}
            >
              <div className="tab-name">{tab.name}</div>
            </div>
          );
        })}
      </div>
    </Tabs>
  );
};

export default TabLayout;
