import styled from "styled-components";

export const Tabs = styled.div`
  cursor: pointer;
  margin-bottom: 15px;
  .tab-options {
    box-shadow: var(--border-white) 0px -0.5px 0px inset;
  }
  .options-wrapper {
    display: flex;
    border-bottom: 2px solid #fff;
  }

  .tab-item {
    /* margin-right: 20px; */
    padding: 2px 20px 2px 20px;
  }

  .tab-name {
    font-size: 13px;
    font-weight: 600;
    margin-left: 10px;

    color: grey;
  }

  /* .active-tab {
    border-bottom: 3px solid gray;
  } */

  .active-tab .tab-name {
    color: #fff;
  }
`;
