import { Container } from "./styled.Blankserver";
import Image from "next/image";
import create from "../../../assete/createnew/create.png";
import React, { useState } from "react";
import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";
import { Checkbox } from "primereact/checkbox";
import { RadioButton } from "primereact/radiobutton";
import { Dropdown } from "primereact/dropdown";
import { InputTextarea } from "primereact/inputtextarea";

const Externalserver = () => {
  // textarea
  const [textarea, setTextarea] = useState("");
  // dynamic radio button
  const Project_categories = [
    {
      Title: "Private",
      Title_Description:
        "Project access must be granted explicitly to each user. If this project is part of a group, access will be granted to members of the group.",
      key: "p1",
    },
    {
      Title: "Public",
      Title_Description:
        "The project can be accessed without any authentication. ",
      key: "p2",
    },
  ];
  const [selectedCategory, setSelectedCategory] = useState(
    Project_categories[1]
  );

  return (
    <Container>
      <div className="create-server-wrapper">
        <div className="create-server-tabs">
          <div classname="create-server-image" style={{ marginBottom: "2em" }}>
            <Image src={create} alt="devops" className="create-s-image" />
          </div>
          <div className="create-server-title" style={{ marginBottom: "2em" }}>
            <h2>Run CI/CD for external repository</h2>
            <h5>Connect your external repository to GitLab CI/CD.</h5>
          </div>
        </div>

        <div className="create-server-tab">
          <ul className="list-none p-0 m-5 flex align-items-center font-medium mb-3">
            <li>
              <a className="text-500 no-underline line-height-3 cursor-pointer">
                New Servers
              </a>
            </li>
            <li className="px-2">
              <i className="pi pi-angle-right text-500 line-height-3"></i>
            </li>
            <li>
              <span className="text-900 line-height-3">Create New Server</span>
            </li>
          </ul>

          <div className="create-server-title" style={{ marginBottom: "3em" }}>
            <h2>Run CI/CD pipelines for external repositories </h2>
            <p>
              Connect your external repositories, and CI/CD pipelines will run
              for new commits. A GitLab project will be created with only CI/CD
              features enabled.{" "}
            </p>
            <p>
              If using GitHub, you’ll see pipeline statuses on GitHub for your
              commits and pull requests. <a href="">More info</a>
            </p>
          </div>
          <div className="p-fluid p-formgrid p-grid">
            <div className="p-field p-col-12 p-md-6">
              <label htmlFor="https:/dokku.company.com/group/server.dokku">
                Dokku repository URL
              </label>
              <br />
              <InputText
                id="project-name"
                type="text"
                placeholder="https:/dokku.company.com/group/server.dokku"
              />
            </div>

            <div className="p-field p-col-12 p-md-6" id="project-Slug">
              <label htmlFor="">Username (optional) </label> <br />
              <InputText id="project-slug-input" type="text" />
            </div>

            <div className="p-field p-col-12 p-md-6" id="project-Slug">
              <label htmlFor=""> Password (optional) </label> <br />
              <InputText id="project-slug-input" type="text" />
            </div>

            <div
              className="documentaries"
              style={{
                borderRadius: "8px",
                backgroundColor: "#F0F8FF",
                color: "#000",
                marginTop: "2rem",
              }}
            >
              <ul className="documentaries-ul">
                <li>
                  The repository must be accessible over http://, https:// or
                  git://.{" "}
                </li>
                <li>
                  If your HTTP repository is not publicly accessible, add your
                  credentials.{" "}
                </li>
                <li>To connect an SVN repository, check out this document. </li>
                <li>
                  The connection will time out after 180 minutes. For
                  repositories that take longer, use a clone/push combination.{" "}
                </li>
                <li>
                  When using the http:// or https:// protocols, please provide
                  the exact URL to the repository. HTTP redirects will not be
                  followed.
                </li>
              </ul>
            </div>

            <div className="p-field p-col-12 ">
              <label htmlFor="description">
                Project description (optional)
              </label>
              <br />
              <InputTextarea
                style={{ padding: "30px" }}
                value={textarea}
                onChange={(e) => setTextarea(e.target.value)}
                placeholder="Description Format"
              />
            </div>

            <div className="select-categories-wrapper">
              <h4 className="select-categories-wrapper-title">
                Visibility Level
              </h4>
              {Project_categories.map((category) => {
                return (
                  <div
                    key={category.key}
                    className="p-field-radiobutton"
                    style={{ margin: "15px" }}
                  >
                    <RadioButton
                      inputId={category.key}
                      name="category"
                      value={category}
                      onChange={(e) => setSelectedCategory(e.value)}
                      checked={selectedCategory.key === category.key}
                    />

                    <label htmlFor={category.key} style={{ margin: "20px" }}>
                      {category.Title}
                    </label>
                    <p style={{ fontSize: "13px" }}>
                      {category.Title_Description}
                    </p>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </Container>
  );
};

export default Externalserver;
