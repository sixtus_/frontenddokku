import styled from "styled-components";

const CreateNewtab = styled.div`
  /* justify-content: center;
  text-align: center; */
  .createtab-header-wrapper {
    margin-top: 80px;
    justify-content: center;
  }
  .wrapper-text {
    text-align: center;
    font-size: 14px;
  }
  .grid {
    justify-content: center;
    width: 100%;
    padding: 5px;
  }

  .col-12 {
    margin-left: 18px;
  }
  .createtab-container {
    background-color: #fff;
    margin: 3px;
    width: 100%;
  }

  .create-icon {
    width: 30%;
    /* height: 100%; */
  }
  .create-icon Image {
    width: 100%;
  }
  .createtab-text-wrapper {
    width: 100%;
  }
  .createtab-title {
    color: black;
    font-weight: 600;
    font-size: 18px;
    padding: 10px;
  }
  .createtab-text {
    color: #000;
    font-weight: 500;
    font-size: 13px;
    padding: 8px;
  }
  /* EXTERNAL STYLING STARTS HERE */
`;
export { CreateNewtab };
