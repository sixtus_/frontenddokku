import {gql} from "@apollo/client";

const getCurrentUser = gql`
    query getCurrentUser {
        getCurrentUser {
            _id
            email
            username
        }
    }
`
export default getCurrentUser
