import React from 'react';
import { useQuery} from "@apollo/client";
import getCurrentUser from "./getCurrentUser";
import LoadingPage from "../Loaders/LoadingPage";
import Router from "next/router";

const CheckAuthForUnAuthPages = ({children}) => {
  const {loading, error} = useQuery(getCurrentUser,{
    onCompleted:()=>Router.push('/dashboard')
  })
  return (
    loading ? <LoadingPage/> : error ? children : <LoadingPage/>
  );
};

export default CheckAuthForUnAuthPages;
