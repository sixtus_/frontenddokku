import styled from "styled-components";

export const Signup = styled.div`
  color: #fff;
  font-weight: 600;
  font-family: "Trebuchet MS", "Lucida Sans Unicode", "Lucida Grande",
    "Lucida Sans", Arial, sans-serif;
  display: flex;
  flex-wrap: wrap;
  padding: 10px;
  display: flex;

  .Signup-wrapper {
    padding: 13px;
    margin-top: 3rem;
    /* display: block; */
    /* flex: 1; */

    .signup-header-h2 {
      font-weight: 600;
      font-size: 30px;
    }
    .signup-header-h5 {
      font-weight: 600;
      font-size: 15px;
    }

    .signup-list .signup-list-item {
      font-size: 13px;
      font-weight: 500;
    }
  }
  h5.p-text-center-dash {
    text-align: center;
    font-weight: 600;
    font-size: 15px;
  }

  .form-auth {
    display: flex;
    align-items: center;
    justify-content: center;
    flex: 1;
    width: 20%;

    /* height: 100vh; */
  }

  .form-auth .card {
    justify-content: center;
    margin: 8px;
    padding: 15px;
  }

  .logo-container {
    justify-content: center;
    width: 100%;
    display: flex;
    .login-logo-wrapper {
      width: 12%;
      .login-logo {
        width: 100%;
        height: 100%;
      }
    }
  }

  .card .p-fluid .p-field {
    padding: 5px;
  }
  .form-auth .card form {
    margin-top: 3rem;
    width: 100%;
  }
  .form-auth .card .p-field {
    margin-bottom: 2rem;
  }

  .p-mt-2 {
    padding: 10px;
    font-size: 14px;
    font-weight: 600;
  }
  .Login-wrapper {
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    cursor: pointer;
    position: relative;
    padding: 0;

    .Login-text-wrapper {
      list-style-type: none;
      font-size: 13px;
      font-weight: 500;

      Link {
        padding-left: 30px;
      }
    }
  }
`;
