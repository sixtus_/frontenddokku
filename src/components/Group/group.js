import React from "react";
import styled from "styled-components";
import LandingBox from "../SeverBox/LandingBox";

const GroupOverview = () => {
  let group = [
    {
      _id: "1",
      groupName: "Group Server1",
      description: "Client and Admin Group server base still on preview",
      active: false,
    },
    {
      _id: "2",
      groupName: "Group Server2 ",
      description: "Client and Admin  group server base still on preview",
      active: false,
    },
    {
      _id: "3",
      groupName: "Group Server3",
      description: "Client and Admin  group server base still on preview",
      active: false,
    },

    {
      _id: "4",
      groupName: "Group Server4",
      description: "Client and Admin  group server base still on preview",
      active: false,
    },
    {
      _id: "5",
      groupName: "Group Server5",
      description: "Client and Admin group server base still on preview",
      active: false,
    },
    {
      _id: "6",
      groupName: "Group Server6",
      description: "Client and Admin server base still on preview",
      active: false,
    },
    {
      _id: "7",
      groupName: "Group Server7",
      description: "Client and Admin group server base still on preview",
      active: false,
    },
    {
      _id: "8",
      groupName: "Group Server9",
      description: "Client and Admin group server base still on preview",
      active: false,
    },
    {
      _id: "9",
      groupName: "Group Server9",
      description: "Client and Admin group server base still on preview",
      active: false,
    },
    {
      _id: "10",
      groupName: "Group Server10",
      description: "Client and Admin group server base still on preview",
      active: false,
    },
    {
      _id: "11",
      groupName: "Group Server11",
      description: "Client and Admin group server base still on preview",
      active: false,
    },
  ];

  // const list = ['s','s','s','s','s','s','s','s','sd','sd','s','r'].map((data)=> <LandingBox  key={data}/>)

  let list = group.map((data) => <LandingBox dataCards={data} />);

  return (
    <Outer>
      <Container>{list}</Container>
    </Outer>
  );
};

const Outer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex: 1;
`;
const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: auto;
  //justify-content: space-around;

  .container {
    content: "";
    flex: auto;
  }
  .tabs {
    margin: auto;
    display: flex;
    flex-wrap: wrap;
    margin: 48px;
    justify-content: space-around;
  }
`;

export default GroupOverview;
