import Head from "next/head";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import "primeflex/primeflex.css";
import "prismjs/themes/prism-tomorrow.css";
import React, { useState, useEffect } from "react";
import classNames from "classnames";
import NProgress from 'nprogress'
import { CSSTransition } from "react-transition-group";
import { AppTopbar } from "../../AppTopbar";
import { AppMenu } from "../../AppMenu";
import PrimeReact from "primereact/api";
import Router from "next/router";
import {gql, useMutation} from "@apollo/client";
PrimeReact.ripple = true;
const logoutMutation = gql`
  mutation logout {
      logout
  }
`
function MainDash({ children }) {
  const [layoutMode] = useState("static");
  const [layoutColorMode] = useState("dark");
  const [staticMenuInactive, setStaticMenuInactive] = useState(false);
  const [overlayMenuActive, setOverlayMenuActive] = useState(false);
  const [mobileMenuActive, setMobileMenuActive] = useState(false);
  const [mobileTopbarMenuActive, setMobileTopbarMenuActive] = useState(false);
  const [logOut] = useMutation(logoutMutation, {
    onCompleted: ()=> Router.push("/")
  })
  let menuClick = false;
  let mobileTopbarMenuClick = false;

  useEffect(() => {
    if (mobileMenuActive) {
      addClass(document.body, "body-overflow-hidden");
    } else {
      removeClass(document.body, "body-overflow-hidden");
    }
  }, [mobileMenuActive]);

  const onWrapperClick = (event) => {
    if (!menuClick) {
      setOverlayMenuActive(false);
      setMobileMenuActive(false);
    }

    if (!mobileTopbarMenuClick) {
      setMobileTopbarMenuActive(false);
    }

    mobileTopbarMenuClick = false;
    menuClick = false;
  };

  const onToggleMenuClick = (event) => {
    menuClick = true;

    if (isDesktop()) {
      if (layoutMode === "overlay") {
        if (mobileMenuActive === true) {
          setOverlayMenuActive(true);
        }

        setOverlayMenuActive((prevState) => !prevState);
        setMobileMenuActive(false);
      }
      // else if (layoutMode === 'static') {
      //   setStaticMenuInactive((prevState) => !prevState);
      // }
      else if (layoutMode === "static") {
        setStaticMenuInactive((prevState) => !prevState);
      }
    } else {
      setMobileMenuActive((prevState) => !prevState);
    }

    event.preventDefault();
  };

  const onSidebarClick = () => {
    menuClick = true;
  };

  const onMobileTopbarMenuClick = (event) => {
    mobileTopbarMenuClick = true;

    setMobileTopbarMenuActive((prevState) => !prevState);
    event.preventDefault();
  };

  const onMobileSubTopbarMenuClick = (event) => {
    mobileTopbarMenuClick = true;

    event.preventDefault();
  };

  const onMenuItemClick = (event) => {
    if (!event.item.items) {
      setOverlayMenuActive(false);
      setMobileMenuActive(false);
    }
  };
  const isDesktop = () => {
    return window.innerWidth >= 992;
  };

  const menu = [
    {
      label: "Overview",
      icon: "pi pi-fw pi-share-alt",
      to: "/",
    },
    {
      icon: "pi pi-fw pi-file",
      items: [
        {
          label: "New",
          icon: "pi pi-fw pi-plus",
          items: [
            {
              label: "New Server",
              icon: "pi pi-fw pi-bookmark",
            },
            {
              label: "New Group Server",
              icon: "pi pi-fw pi-video",
            },
          ],
        },
        {
          label: "Milistones",
          icon: "pi pi-fw pi-trash",
        },

        {
          label: "Snippet",
          icon: "pi pi-fw pi-external-link",
        },
      ],
    },
    {
      items: [
        {
          label: "Activity",
          icon: "pi pi-fw pi-home",
          items: [
            {
              label: "ENvironments",
              icon: "pi pi-fw  pi-cloud",
            },
            {
              label: "Analytics View",
              icon: "pi pi-fw  pi-sitemap",
            },
            {
              label: "Security",
              icon: "pi pi-fw  pi-sitemap",
            },
          ],
        },
        {
          label: "Solve an issue",
          icon: "pi pi-fw pi-exclamation-circle",
          items: [
            {
              label: "Report a problem",
              icon: "pi pi-fw   pi-comments",
            },
            {
              label: "Support",
              icon: "pi pi-fw  pi-question-circle",
            },
            {
              label: "Policies",
              icon: "pi pi-fw  pi-external-link",
            },
            {
              label: "user Validity",
              icon: "pi pi-fw  pi-shield",
            },
          ],
        },

        {
          label: "Settings",
          icon: "pi pi-fw pi-cog",
        },
        {
          label: 'Sign-out',
          to:'/',
          icon: 'pi pi-fw pi-sign-out',
          command: () => {
           NProgress.start()
            logOut().catch(()=>{});
          },
        }
      ],
    },
  ];

  const addClass = (element, className) => {
    if (element.classList) element.classList.add(className);
    else element.className += " " + className;
  };

  const removeClass = (element, className) => {
    if (element.classList) element.classList.remove(className);
    else
      element.className = element.className.replace(
        new RegExp(
          "(^|\\b)" + className.split(" ").join("|") + "(\\b|$)",
          "gi"
        ),
        " "
      );
  };

  const wrapperClass = classNames("layout-wrapper", {
    "layout-static": layoutMode === "static",
    "layout-static-sidebar-inactive":
      staticMenuInactive && layoutMode === "static",
    // 'layout-static': layoutMode === 'static',
    // 'layout-static-sidebar-inactive': staticMenuInactive && layoutMode === 'static',
    "layout-overlay-sidebar-active":
      overlayMenuActive && layoutMode === "overlay",
    "layout-mobile-sidebar-active": mobileMenuActive,
    "p-input-filled": false,
    "p-ripple-disabled": false,
    "layout-theme-dark": "dark",
  });
  return (
    <>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        {/*<title>React App</title>*/}
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta
          name="description"
          content="Web site created using create-next-app"
        />
        <meta name="theme-color" content="#000000" />
      </Head>

      <div className={wrapperClass} onClick={onWrapperClick}>
        <AppTopbar
          onToggleMenuClick={onToggleMenuClick}
          layoutColorMode={layoutColorMode}
          mobileTopbarMenuActive={mobileTopbarMenuActive}
          onMobileTopbarMenuClick={onMobileTopbarMenuClick}
          onMobileSubTopbarMenuClick={onMobileSubTopbarMenuClick}
        />

        <div className="layout-sidebar" onClick={onSidebarClick}>
          <AppMenu
            model={menu}
            onMenuItemClick={onMenuItemClick}
            layoutColorMode={layoutColorMode}
          />
        </div>
        <div className="layout-main-container">
          <div className="layout-main">{children}</div>
        </div>

        <CSSTransition
          classNames="layout-mask"
          timeout={{ enter: 200, exit: 200 }}
          in={mobileMenuActive}
          unmountOnExit
        >
          <div className="layout-mask p-component-overlay" />
        </CSSTransition>
      </div>
    </>
  );
}
const MainContainer = (props) => {
  return <MainDash {...props} />;
};
export default MainContainer;
