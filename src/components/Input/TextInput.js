import React from 'react';
import {InputText} from "primereact/inputtext";
import {classNames} from "primereact/utils";
import styled from "styled-components";

const TextInput = ({onChange, error,placeholder, name, value, inputType,icon, ...props}) => {
  return (
    <>
      <Container className="p-field">
                <span className="p-float-label p-input-icon-right">
                  {icon && <i className={`pi ${icon}`}/>}
                  <InputText
                    id={name}
                    name={name}
                    type={inputType}
                    value={value}
                    onChange={onChange}
                    {...props}
                    className={classNames({
                      "p-invalid": error[name],
                    })}
                    style={{ height: "47px", fontSize: "14px" }}
                  />
                  <label
                    htmlFor={name}
                    className={classNames({
                      "p-error": error[name],
                    })}
                    style={{ fontSize: "15px", fontWeight: "600" }}
                  >
                    {placeholder}
                  </label>
                </span>
      </Container>
    </>
    
  );
};
const Container= styled.div`
  margin-bottom: 0 !important;
  > span {
    width:100%;
  }
`
export default TextInput;
