
import { Password } from 'primereact/password';

import {classNames} from "primereact/utils";
import styled from "styled-components";
import React from "react";

const TextInput = ({onChange, error,placeholder, icon, name, value, inputType, ...props}) => {
  
  return (
    <>
      <Container className="p-field">
                <span className="p-float-label p-input-icon-right">
                    {icon && <i className={`pi ${icon}`}/>}
                  <Password
                    id={name}
                    {...props}
                    name={name}
                    type={inputType}
                    value={value}
                    onChange={onChange}
                    className={classNames({
                      "p-invalid": error[name],
                    })}
                    style={{ height: "47px", fontSize: "14px" }}
                  />
                  <label
                    htmlFor={name}
                    className={classNames({
                      "p-error": error[name],
                    })}
                    style={{ fontSize: "15px", fontWeight: "600" }}
                  >
                    {placeholder}
                  </label>
                </span>
      </Container>
    </>
  
  );
};
const Container= styled.div`
  margin-bottom: 0 !important;
  > span {
    width:100%;
  }
`
export default TextInput;
