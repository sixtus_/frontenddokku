import React from "react";
import * as Yup from 'yup'
import Image from "next/image";
import logo from "../../assete/logo.png";
import {Container} from "./styled.login";
import Link from "next/link";
import {gql, useMutation} from "@apollo/client";
import TextInput from "../Input/TextInput";
import PasswordInput from "../Input/PasswordInput";
import CustomForm from "../CustomForm/Form";
import getCurrentUser from "../AuthControl/getCurrentUser";
import Router from "next/router";

const initialSchemaAccountInformation = Yup.object().shape({
  email: Yup.string().required().email("Invalid email address. E.g. example@email.com"),
  password: Yup.string().required().min(4)
});
const AccountInformationInput = [
  {
    placeholder: 'Email*',
    name: 'email',
    inputType: 'email',
    as: TextInput,
    icon:"pi-envelope"
  },
  {
    placeholder: 'Password*',
    name: 'password',
    inputType: 'password',
    as: PasswordInput,
    options: {feedback:false, toggleMask:true }
  },
];

const accountInformation = {
  email: '',
  password: ''
};
const loginUserMutation = gql`
    mutation loginUser ($email:EmailAddress! $password:String!){
        loginUser(
            email: $email
            password: $password
        )
    }
`
export default function LoginPage() {
  const [loginUser, {loading}] = useMutation(loginUserMutation, {
    onCompleted: () => {
      if(window.location.href.includes("/login")) {
        console.log(window.location.href)
        Router.push('/dashboard')
      }
    },
    refetchQueries: [{query: getCurrentUser}],
    awaitRefetchQueries: true
  })
  return (
    <Container>
      <div className="form-auth">
        <div className="p-d-flex p-jc-center">
          <div className="card">
            <div className="logo-container">
              <div className="login-logo-wrapper">
                <Image src={logo} className="login-logo" />
              </div>
            </div>
            <h2 className="p-text-center-dash center-text">Welcome to DSPM</h2>
            <h5 className="p-text-center-dash">Login</h5>
            <CustomForm
              alignItems="flex-end !important"
              inputWidth="100%"
              onSubmitFunction={async (fields) => {
                loginUser({variables:fields}).catch(()=>{})
              }}
              initialSchema={initialSchemaAccountInformation}
              initialValues={accountInformation}
              title=""
              infoText=""
              btnText={loading ? 'please wait' : 'Sign In'}
              loading={loading}
              formInputs={AccountInformationInput}
            />
          </div>
          <div className="p-field">
            <ul className="Login-wrapper">
              <li className="Login-text-wrapper">
                    <span>
                      Don't have an Account yet?
                      <Link href="/signup"> Sign Up</Link>
                    </span>
              </li>
              <li className="Login-text-wrapper">
                <Link href="/forgotpassword">Forgot your Password?</Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </Container>
  );
}
