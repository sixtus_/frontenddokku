import React from "react";
import { Button } from "primereact/button";
import { Card } from "primereact/card";
import styled from "styled-components";
import CheckStatus from "../Indicator/checkStatus";
import { useRouter } from "next/router";

const LandingBox = ({ dataCards }) => {
  const router = useRouter();

  const footer = (
    <span>
      <Button
        label="Mange Server "
        onClick={() =>
          router.push({
            pathname: `${dataCards.serverName ? "servers" : "groups"}/${
              dataCards._id
            }`,
          })
        }
        style={{ fontSize: "12px" }}
      />
    </span>
  );

  return (
    <BoxContainer>
      <Card
        title={
          <TitleContainer>
            {/* <img
              alt="Card"
              src="showcase/demo/images/usercard.png"
              onError={(e) =>
                (e.target.src =
                  "https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png")
              }
              style={{ width: "20px" }}
            /> */}

            <div style={{ fontSize: "14px" }}>
              {dataCards && dataCards.serverName
                ? dataCards.serverName
                : dataCards.groupName}
            </div>
            <CheckStatus status="success" />
          </TitleContainer>
        }
        subTitle="Dokku Servers"
        style={
          dataCards.groupName
            ? {
                backgroundColor: "darkslategray",
                width: "12em",
              }
            : { width: "12em" }
        }
        footer={footer}
        header={null}
      >
        <p className="p-m-0" style={{ lineHeight: "1", fontSize: "12px" }}>
          {dataCards.description}
        </p>
      </Card>
    </BoxContainer>
  );
};

const TitleContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;
const BoxContainer = styled.div`
  display: flex;
  padding: 5px;
  border-radius: 10px;
  justify-content: center;
`;
export default LandingBox;
