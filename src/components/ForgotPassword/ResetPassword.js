import { ForgotPasword } from "./styled.Forgot";
import React from "react";
import Image from "next/image";
import logo from "../../assete/logo.png";
import CustomForm from "../CustomForm/Form";
import * as Yup from "yup";
import PasswordInput from "../Input/PasswordInput";
import {useRouter} from "next/router";
import {gql, useMutation} from "@apollo/client";

const inputElements = [
  {
    placeholder: 'Enter New Password',
    name: 'password',
    inputType: 'password',
    as: PasswordInput,
    options: {feedback:false, toggleMask:true }
  },
  {
    placeholder: 'Confirm New Password',
    name: 'confirmPassword',
    inputType: 'password',
    as: PasswordInput,
    options: {feedback:false, toggleMask:true }
  }
];

const initialSchema = Yup.object().shape({
  password: Yup.string()
    .min(6, 'Password must be at least 6 characters')
    .required('Password is required'),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref('password'), null], 'Passwords must match')
    .required('Confirm Password is required'),
});

const initialValues = {
  password: '',
  confirmPassword: '',
};

const resetPassword = gql`
  mutation resetPassword($token:String! $password:String!) {
      resetPassword(token: $token password: $password)
  }
`
const ResetPassword = () => {
  const router = useRouter()
  const [UpdatePassword, { loading }] = useMutation(resetPassword, {
    variables: {token:router.query.token},
    onCompleted: () => {
      router.push('/dashboard').catch(()=>{});
    },
  });
  return (
    <ForgotPasword>
      <div className="form-auth">
        <div className="p-d-flex p-jc-center">
          <div className="card">
          <div className="logo-container">
              <div className="login-logo-wrapper">
                <Image src={logo} className="login-logo" />
              </div>
            </div>
              <h2 className="p-text-center-dash" style={{ fontSize: "18px" }}>
                Update password
              </h2>
              <p
                style={{
                  fontWeight: "600",
                  color: "grey",
                  padding: "2px",
                  fontSize: "12px",
                }}
              >
              Updated you account with a new password
                <br />
                and you will get instant accesss to you dashboard after that.
              </p>
              <CustomForm
                alignItems="flex-end !important"
                inputWidth="100%"
                onSubmitFunction={async (fields) => {
                  UpdatePassword({variables: fields}).catch(() => {
                  })
                }}
                initialSchema={initialSchema}
                initialValues={initialValues}
                title=""
                infoText=""
                btnText={loading ? 'please wait' : 'Update Password'}
                loading={loading}
                formInputs={inputElements}
              />
          </div>
        </div>
      </div>
    </ForgotPasword>
  );
};

export default ResetPassword;
