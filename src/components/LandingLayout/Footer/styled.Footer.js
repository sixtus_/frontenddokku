import styled from "styled-components";

export const Footerpage = styled.div`
  cursor: pointer;
  text-align: center;
  width: 100%;
  .logo {
    text-align: center;
    display: block;
    width: 100%;
  }
  .tabs {
    display: flex;
    justify-content: center;
    padding: 25px;
    flex-wrap: wrap;
  }

  .cards {
    /* display: flex; */
    justify-content: center;
    text-align: center;
    padding: 10px;
    flex-direction: column;
  }
  .list-none {
    justify-content: center;
  }
  .list-none h2 {
    font-size: 18px;
  }
  .pi-check-circle {
    font-size: 12px;
  }
  span {
    font-size: 12px;
  }
  .list-none .flex:hover {
    color: #000;
  }
`;
