import styled from "styled-components";

export const FixVal = styled.div`
  /* padding: 5px; */
  position: relative;
  flex-wrap: wrap;
  justify-content: center;
  justify-content: space-around;
  position: relative;
  /* background: #191970; */
  background-color: rgb(43, 43, 178);
  color: rgb(255, 255, 255);
  font-weight: 600;
  /* box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, 0.2); */
  /* border-radius: 10px; */
  cursor: pointer;
  text-align: center;

  .tabs {
    margin: auto;
    display: flex;
    flex-wrap: wrap;
    /* margin: 40px; */
    justify-content: space-around;
  }
  .Fixval-text {
    padding: 18px;
  }
  .Fixval-text h2 {
    font-size: 20px;
    font-weight: 700;
  }
  .text-gradient-coral-yellow-dark {
    color: #ffe57f;
    font-size: 15px;
    font-weight: 600;
    /* color: #9867f0; */
    /* font-size: 37px; */
  }
  span {
    color: #ffe57f;
    font-size: 15px;
  }
  .p-m-0 {
    font-size: 13px;
  }
  a {
    color: white;
    /* text-decoration: underline  1px solid white; */
    font-size: 13px;
    letter-spacing: 2px;
  }
  a:hover {
    text-decoration: underline 2px solid #ffe57f;
  }
`;
