import React from "react";
import classNames from "classnames";
// import { div } from 'primereact/card';
import { Hometabs } from "./styled.tabs";
// import images
// import Image from 'next/image'
import Image from "next/image";
import devops from "../../../../assete/devops.svg";
import solutions from "../../../../assete/solutions.png";
import create from "../../../../assete/create.png";
import server from "../../../../assete/server.jpg";
import verify from "../../../../assete/landingpage/verify.png";
import secure from "../../../../assete/landingpage/secure.png";
import dokku from "../../../../assete/landingpage/dokku.png";

const MultiTab = () => {
  return (
    <Hometabs>
      <div className="manage-content-container">
        <div className="manage-image" style={{ height: "130px" }}>
          <Image src={server} />
        </div>

        <div className="manage-copy-wrapper">
          <div className="title">
            <h4> Manage your toolchain before it manages you </h4>
          </div>
          <div className="content">
            <p>
              Reduce toolchain complexity for improved software delivery and
              quality.
            </p>
          </div>
          <div className="cta-wrapper">
            <div className="cta-text">Download the report today</div>
          </div>
        </div>
      </div>

      <div className="tabs">
        <div
          title="How DevOps is better with Dokku"
          className="cards"
          style={{ width: "36rem", marginBottom: "2em" }}
        >
          <h4>Manage</h4>
          <h6>
            Gain visibility and insight into how your business is performing.
          </h6>
          <p className="p-m-0" style={{ lineHeight: "1.5" }}>
            Dokku helps teams manage and optimize their software delivery
            lifecycle with metrics and value stream insight in order to
            streamline and increase their delivery velocity. Learn more about
            how Dokku helps to manage your end to end value stream.
          </p>
        </div>
        <div title="Simple div" style={{ width: "23rem", marginBottom: "2em" }}>
          <Image src={devops} alt="devops" className="landingpage-img" />
        </div>
      </div>

      <div className="tabs">
        <div style={{ width: "23rem", marginBottom: "2em" }}>
          <Image src={solutions} alt="create" className="landingpage-img" />
        </div>

        <div className="cards" style={{ width: "36rem", marginBottom: "2em" }}>
          <h4>Plans</h4>
          <p className="p-m-0" style={{ lineHeight: "1.5" }}>
            <h6>
              {" "}
              Regardless of your process, Dokku provides powerful planning tools
              to keep everyone synchronized.
            </h6>
            Dokku enables portfolio planning and management through epics,
            groups (programs) and milestones to organize and track progress.
            Regardless of your methodology from Waterfall to DevOps, GitLab’s
            simple and flexible approach to planning meets the needs of small
            teams to large enterprises. Dokku helps teams organize, plan, align
            and track project work to ensure teams are working on the right
            things at the right time and maintain end to end visibility and
            traceability of issues throughout the delivery lifecycle from idea
            to production.
          </p>
        </div>
      </div>

      <div className="tabs">
        <div
          title="How DevOps is better with Dokku"
          className="cards"
          style={{ width: "36rem", marginBottom: "2em" }}
        >
          <h4>Create</h4>
          <h6>
            GCreate, view, and manage code and project data through powerful
            branching tools..
          </h6>
          <p className="p-m-0" style={{ lineHeight: "1.5" }}>
            Dokku helps teams design, develop and securely manage code and
            project data from a single distributed version control system to
            enable rapid iteration and delivery of business value. Dokku
            repositories provide a scalable, single source of truth for
            collaborating on projects and code which enables teams to be
            productive without disrupting their workflows.
          </p>
        </div>
        <div title="Simple div" style={{ width: "23rem", marginBottom: "2em" }}>
          <Image src={create} alt="devops" className="landingpage-img" />
        </div>
      </div>

      <div className="tabs">
        <div title="Simple div" style={{ width: "23rem", marginBottom: "2em" }}>
          <Image src={verify} alt="create" className="landingpage-img" />
        </div>
        <div className="cards" style={{ width: "36rem", marginBottom: "2em" }}>
          <h4>Verify</h4>
          <p className="p-m-0" style={{ lineHeight: "1.5" }}>
            <h6>
              Keep strict quality standards for production code with automatic
              testing and reporting.{" "}
            </h6>
            Dokku helps delivery teams fully embrace continuous integration to
            automate the builds, integration and verification of their code.
            GitLab’s industry leading CI capabilities enables automated testing,
            Static Analysis Security Testing, Dynamic Analysis Security testing
            and code quality analysis to provide fast feedback to developers and
            testers about the quality of their code. With pipelines that enable
            concurrent testing and parallel execution, teams quickly get insight
            about every commit, allowing them to deliver higher quality code
            faster.
          </p>
        </div>
      </div>

      <div className="tabs">
        <div className="cards" style={{ width: "36rem", marginBottom: "2em" }}>
          <h4>Package</h4>
          <p className="p-m-0" style={{ lineHeight: "1.5" }}>
            <h6>
              Keep strict quality standards for production code with automatic
              testing and reporting.{" "}
            </h6>
            Dokku enables teams to package their applications and dependencies,
            manage containers, and build artifacts with ease. The private,
            secure, container and package registry are built-in and
            preconfigured out-of-the box to work seamlessly with Dokku source
            code management and CI/CD pipelines. Ensure DevOps acceleration and
            a faster time to market with automated software pipelines that flow
            freely without interruption.
          </p>
        </div>
        <div title="Simple div" style={{ width: "23rem", marginBottom: "2em" }}>
          <Image src={secure} alt="create" className="landingpage-img" />
        </div>
      </div>

      <div className="tabs">
        <div title="Simple div" style={{ width: "23rem", marginBottom: "2em" }}>
          <Image src={secure} alt="create" className="landingpage-img" />
        </div>

        <div className="cards" style={{ width: "36rem", marginBottom: "2em" }}>
          <h4>PAckage</h4>
          <p className="p-m-0" style={{ lineHeight: "1.5" }}>
            <h6>
              Security capabilities, integrated into your development lifecycle.{" "}
            </h6>
            Dokku provides Static Application Security Testing (SAST), Dynamic
            Application Security Testing (DAST), Container Scanning, and
            Dependency Scanning to help you deliver secure applications along
            with license compliance.
          </p>
        </div>
      </div>
    </Hometabs>
  );
};

export default MultiTab;
