import React from "react";
import Image from "next/image";
import { FixVal } from "./styled.Fix";
import codeql from "../../../../assete/landingpage/codeql.webp";
import secret from "../../../../assete/landingpage/secret.webp";
import enterprise from "../../../../assete/landingpage/enterprise.webp";

const FixVulnerabilities = () => {
  return (
    <FixVal>
      <div className="Fixval-text">
        <h2>
          Find and Fix vulnerabilities <br />
          <span className="text-gradient-coral-yellow-dark ">
            before you merge{" "}
          </span>
        </h2>
        <a className=""> Learn more about advanced security </a>{" "}
        <i className="pi pi-chevron-right" />
      </div>

      <div className="tabs">
        <div className="cards" style={{ width: "30rem", marginBottom: "2em" }}>
          <h6 className="js-build-in-item build-in-slideX-left f2-mktg text-gray-mktg lh-condensed text-semibold mb-5 mb-lg-0 col-5-max build-in-animate">
            <span className="text-gray-dark-mktg">
              Better code starts with pull requests
            </span>
            —conversations around your code where you can experiment, squash
            bugs, and build new features.
          </h6>

          <h6>
            Gain visibility and insight into how your business is performing.
          </h6>
          <p className="p-m-0" style={{ lineHeight: "1.5" }}>
            <span className="color-fg-default">
              {" "}
              Secure your code as you write it ?{" "}
            </span>
            CodeQL’s code scanning automatically reviews every change to your
            codebase and identifies known vulnerabilities before they ever reach
            production.
          </p>
        </div>
        <div title="Simple div" style={{ width: "23rem", marginBottom: "2em" }}>
          <Image src={codeql} alt="devops" className="landingpage-img" />
        </div>
      </div>

      <div className="tabs">
        <div className="cards" style={{ width: "30rem", marginBottom: "2em" }}>
          <h6 className="js-build-in-item build-in-slideX-left f2-mktg text-gray-mktg lh-condensed text-semibold mb-5 mb-lg-0 col-5-max build-in-animate">
            <span className="text-gray-dark-mktg">
              Better code starts with pull requests
            </span>
            —conversations around your code where you can experiment, squash
            bugs, and build new features.
          </h6>

          <h6>
            Gain visibility and insight into how your business is performing.
          </h6>
          <p className="p-m-0" style={{ lineHeight: "1.5" }}>
            <span className="color-fg-default">Keep your secrets. ? </span>
            We automatically scan repositories for OAuth tokens, API keys,
            personal tokens, and more. If we find one, we’ll notify you and the
            partner that issued it to invalidate the secret.
          </p>
        </div>
        <div title="Simple div" style={{ width: "23rem", marginBottom: "2em" }}>
          <Image src={secret} alt="devops" className="landingpage-img" />
        </div>
      </div>

      <div className="tabs">
        <div className="cards" style={{ width: "30rem", marginBottom: "2em" }}>
          <h6 className="js-build-in-item build-in-slideX-left f2-mktg text-gray-mktg lh-condensed text-semibold mb-5 mb-lg-0 col-5-max build-in-animate">
            <span className="text-gray-dark-mktg">
              Better code starts with pull requests
            </span>
            —conversations around your code where you can experiment, squash
            bugs, and build new features.
          </h6>

          <h6>
            Gain visibility and insight into how your business is performing.
          </h6>
          <p className="p-m-0" style={{ lineHeight: "1.5" }}>
            <span className="color-fg-default">Found a vulnerability? </span>
            Our security advisory remediation tools help developers identify and
            disclose them responsibly so maintainers can patch them in
            dedicated, private workspaces.
          </p>
        </div>
        <div title="Simple div" style={{ width: "23rem", marginBottom: "2em" }}>
          <Image src={secret} alt="devops" className="landingpage-img" />
        </div>
      </div>

      <div className="tabs">
        <div className="cards" style={{ width: "30rem", marginBottom: "2em" }}>
          <h6 className="js-build-in-item build-in-slideX-left f2-mktg text-gray-mktg lh-condensed text-semibold mb-5 mb-lg-0 col-5-max build-in-animate">
            <span className="text-gray-dark-mktg">
              Better code starts with pull requests
            </span>
            —conversations around your code where you can experiment, squash
            bugs, and build new features.
          </h6>

          <h6>
            Gain visibility and insight into how your business is performing.
          </h6>
          <p className="p-m-0" style={{ lineHeight: "1.5" }}>
            <span className="color-fg-default">Found a vulnerability? </span>
            Our security advisory remediation tools help developers identify and
            disclose them responsibly so maintainers can patch them in
            dedicated, private workspaces.
          </p>
        </div>
        <div
          title="Simple div"
          style={{ width: "23rem", marginBottom: "2em" }}
        ></div>
      </div>
    </FixVal>
  );
};

export default FixVulnerabilities;
