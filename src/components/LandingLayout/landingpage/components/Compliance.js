import React from "react";
import Image from "next/image";
import { Achievement } from "./styled.compliance";
import speed from "../../../../assete/slide/speed.svg";
import lessrisk from "../../../../assete/slide/lessrisk.svg";
import efficiency from "../../../../assete/slide/efficiency.svg";

const Compliance = () => {
  return (
    <Achievement>
      <div className="text-blue-600 font-bold m-3">
        <i className="pi pi-discord"></i>&nbsp;POWERED BY DOKKU
      </div>
      <h3>DOKKU is The DevOps Platform.</h3>
      <h4>
        Our users have spoken. Dokku ranks as a G2 Leader across DevOps
        categories..
      </h4>

      <div className="grid">
        <div className="col-12 md:col-4 mb-4 px-5">
          <span
            className="p-3 shadow-2 mb-3 inline-block"
            style={{ borderRadius: "10px", width: "30%" }}
          >
            <Image src={speed} />
          </span>
          <h3>Deliver better software faster</h3>
          <span>Focus on delivering value–not maintaining integrations..</span>
        </div>

        <div className="col-12 md:col-4 mb-4 px-5">
          <span
            className="p-3 shadow-2 mb-3 inline-block"
            style={{ borderRadius: "10px", width: "30%" }}
          >
            <Image src={lessrisk} />
          </span>
          <h3>Reduce risk and cost</h3>
          <span>
            Automate security and compliance without compromising speed or
            spend...
          </span>
        </div>

        <div className="col-12 md:col-4 mb-4 px-5">
          <span
            className="p-3 shadow-2 mb-3 inline-block"
            style={{ borderRadius: "10px", width: "30%" }}
          >
            <Image src={efficiency} />
          </span>
          <h3>Work more efficiently</h3>
          <span>
            Identify blockers and address them immediately, in a single tool...
          </span>
        </div>
      </div>
    </Achievement>
  );
};

export default Compliance;
