import React from "react";
import { Tabs } from "./styled.Landingpage";

import MultiTab from "./components/Tabs";
import FixVulnerabilities from "./components/FixVulnerabilities";
import Oneproduct from "./components/OneProduct";
import Compliance from "./components/Compliance";

const HomeTabs = () => {
  return (
    <Tabs>
      <MultiTab />
      <FixVulnerabilities />
      <Compliance />
      <Oneproduct />
    </Tabs>
  );
};

export default HomeTabs;
