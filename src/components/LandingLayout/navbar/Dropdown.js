import React, { useState } from "react";
// import MenuItems from "./MenuItems";
import { MenuItems } from "./Menuitems";
import { DropdownMenu } from "./styled.Dropdown";
import Link from "next/link";
function Dropdown() {
  const [click, setClick] = useState(false);

  const handleClick = () => setClick(!click);

  return (
    <DropdownMenu>
      <ul
        onClick={handleClick}
        className={click ? "dropdown-menu clicked" : "dropdown-menu"}
      >
        {MenuItems.map((item, index) => {
          return (
            <li key={index} className="dropdown-list">
              <Link
                className={item.cName}
                href={item.path}
                onClick={() => setClick(false)}
              >
                {item.title}
              </Link>
              <br />
              <br />
              <span style={{ color: "black" }}>
                <Link
                  className={item.cName}
                  href={item.path}
                  onClick={() => setClick(false)}
                >
                  {item.text}
                </Link>
              </span>
            </li>
          );
        })}
      </ul>
    </DropdownMenu>
  );
}

export default Dropdown;
