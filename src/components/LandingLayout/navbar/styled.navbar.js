import styled from "styled-components";

export const Nav = styled.div`
  background: #1c2237;
  height: 60px;
  display: flex;
  align-items: center;
  position: sticky;
  top: 0;
  z-index: 999;
  background-color: rgb(31, 45, 64)
  /* background: #232d64; */
  /* background-color: rgb(43, 43, 178); */
  /* color: rgb(255, 255, 255); */
  font-weight: 600;

  .navbar-container {
    display: flex;
    height: 70px;
    /* justify-content: space-between; */
  }

  .container {
    z-index: 1;
    width: 100%;
    max-width: 1500px;
    /* margin-right: auto;
    margin-left: auto; */
    padding-right: 15px;
    padding-left: 30px;
  }

  .logo-container {
    justify-content: center;
    width: 15%;
    padding: 25px;
    .login-logo-wrapper {
      width: 18%;
      .login-logo {
        width: 100%;
        height: 100%;
      }
    }
  }

  .nav-menu {
    display: flex;
    align-items: center;
    justify-content: space-around;
    list-style: none;
    text-align: center;
    justify-content: end;
  }

  .nav-item {
    height: 70px;
    border-bottom: 2px solid transparent;
    padding: 30px;
    font-size: 12px;
  }

  .nav-links {
    color: #fff;
    display: flex;
    align-items: center;
    text-decoration: none;
    /* height: 100%; */
  }

  .fa-bars {
    color: #fff;
  }

  .menu-icon {
    display: none;
  }
  .pi-chevron-down {
    font-size: 11px;
    padding-left: 5px;
  }

  @media screen and (max-width: 961px) {
    .logo-container {
      justify-content: center;
      width: 25%;
      padding: 18px;
      .login-logo-wrapper {
        width: 30%;
        .login-logo {
          width: 100%;
          height: 100%;
        }
      }
    }
    .NavbarItems {
      position: relative;
    }

    .nav-menu {
      display: flex;
      flex-direction: column;
      width: 100%;
      height: 30vh;
      position: absolute;
      top: 60px;
      padding: 5px;
      font-weight: 600;
      right: -100%;
      opacity: 1;
      transition: all 0.5s ease;
    }

    .nav-menu.active {
      background: #1c2237;
      left: 0;
      opacity: 1;
      transition: all 0.6s ease;
      z-index: 1;
    }

    .nav-links {
      text-align: start;
      padding: 2rem;
      width: 100%;
      display: table;
    }

    .nav-links:hover {
      color: #f00946;
      transform: scale(1.2);
      transition: all 0.3s ease;
    }

    .nav-item {
      width: 100%;
      border-radius: 7px;
      margin: 2px;
      padding: 10px;
    }

    .navbar-logo {
      position: absolute;
      top: 0;
      left: 0;
      transform: translate(25%, 50%);
    }

    .menu-icon {
      display: block;
      position: absolute;
      top: 0;
      right: 0;
      transform: translate(-100%, 60%);
      font-size: 1.8rem;
      cursor: pointer;
    }

    .fa-times {
      color: #fff;
      font-size: 2rem;
    }

    .nav-btn {
      display: flex;
      justify-content: center;
      align-items: center;
      width: 100%;
      height: 120px;
    }
  }

  @media screen and (max-width: 428px) {
    .logo-container {
      justify-content: center;
      width: 23%;
      padding-top: 18px;

      .login-logo-wrapper {
        width: 80%;
        .login-logo {
          width: 100%;
          height: 100%;
        }
      }
    }
  }
  @media screen and (max-width: 384px) {
    .logo-container {
      justify-content: center;
      width: 30%;
      padding-top: 25px;
      .login-logo-wrapper {
        width: 50%;
        .login-logo {
          width: 100%;
          height: 100%;
        }
      }
    }
  }
`;
