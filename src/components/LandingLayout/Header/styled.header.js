import styled from "styled-components";

export const Header = styled.div`
  justify-content: center;
  /* background-color: #5603ad; */
  justify-content: space-around;
  position: relative;
  .Header-container {
    /* height: 100%; */
    justify-content: center;
    /* background-color: #5603ad; */
    justify-content: space-around;
    position: relative;
  }
  .Container {
    width: 100%;
    display: flex;
    justify-content: space-between;
  }

  /* static image */
  .blank-header {
    overflow: hidden;
    display: flex;
  }

  .blank-header .image-border {
    top: 0;
    height: 100%;
    opacity: 0.2;
  }

  .content-center {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    text-align: center;
    color: #fff;
    padding: 10 10px;
    width: 100%;
    max-width: 890px;
  }

  .content-title {
    font-size: 1.5em;
    line-height: 1.05;
    margin-bottom: 25px;
    color: #fff;
    font-weight: 600;
    text-align: center;
    text-transform: capitalize;
  }
  .content-sub-title {
    font-size: 1rem;
    font-size: 600;
  }

  @media screen and (max-width: 640px) {
    .Container {
      width: 100%;
      /* position: relative; */
    }
    .content-center {
      padding: 20px;
      padding-top:4rem
      height: 100%;
    }
    .content-title {
      font-size: 25px;
      /* padding-top: 8rem; */
    }
    .content-sub-title {
      font-size: 15px;
    }
  }
  @media screen and (max-width: 320px){
    .Container {
      width: 100%;
      /* position: relative; */
    }
    .content-center {
      padding: 25px;
      height: 100%;
    }
    .content-title {
      font-size: 15px;
    }
    .content-sub-title {
      font-size: 12px;
    }
  }
`;
