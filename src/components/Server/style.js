import styled from "styled-components";

const Card = styled.div`
  border: 5px dotted #bbb;
  width: 80%;
  border-radius: 15px;
  margin: 0 auto;
  max-width: 600px;
  .list-none {
  }
  .container {
    padding: 2px 16px;
    background-color: #f1f1f1;
  }

  .promo {
    background: #ccc;
    padding: 3px;
  }

  .expire {
    color: red;
  }
`;
export { Card };
